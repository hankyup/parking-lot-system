# Parking Lot System #

Build a module for a parking garage system that assigns incoming cars a space to park. Assume there is a sensor at the gates to detect a car’s arrival and departure as well as a sensor at each space that detects when that space becomes occupied by a car as well as when it leaves. Cars are identified by their license plate number. Your task is to build the logic necessary to assign a parking space to an incoming car based on the current availability in the garage.

Here are the business requirements:
1. There are a very large, but finite number of spaces in the parking garage.
1. Parking spaces are assigned a weight that reflects their desirability (ex. closer to an elevator is good). Higher weight is better.
1. When a car enters the garage, it must be presented with a space to park in. The space should be the most desirable yet available spot. Calculating this spot should be done efficiently.
1. Drivers are unpredictable. You will receive a notification when they park in or leave a space and when they exit the garage. They may choose not to park in the assigned spot.

As a member of the team, you are responsible for implementing the SpaceAssigner class below. You have the following requirements:
1. Implement the SpaceAssigner class. Implement any additional classes only if required. Code should be production quality – clearly written, runnable, and documented.
1. You are free to use any of the provided interfaces, though you may not modify them in any way. You also may not assume any implementation details of an interface beyond what has been documented.
1. The space returned by assignSpace() should be the space with the highest value of getDesirability() among the list of available spaces.
1. If no spaces are available, a null value should be returned from assignSpace().
1. This application will be run on a device with limited memory and processing resources.
1. You are free to use any tools such as IDEs to compile and test your code.
1. You may use any functions or classes from the JDK, STL, or .Net Framework. You may use a unit testing framework. Do not include extraneous code that is not relevant to your solution.
1. Assumptions other than those made above should be stated clearly at the top of your solution.
1. Write test cases that prove your solution and call your functions to demonstrate that it works.
1. Provide an analysis of the runtime and space complexity (memory usage) in big O notation for all the public APIs in SpaceAssigner. The analysis should include the average and worst-case complexity along with a brief explanation of your reasoning.








```
#!cpp

/**
 * The SpaceAssigner is responsible for assigning a space for an incoming
 * car to park in. This is done by calling the assignSpace() API.
 *
 * The SpaceAssigner responds to changes in space availability by 
 * implementing the GarageStatusListener interface.
 */
public class SpaceAssigner implements GarageStatusListener
{
  /**
   * Initiates the SpaceAssigner. This method is called only once per
   * app start-up.
   * @param garage The parking garage for which you are vending spaces.
   *
   * <<insert runtime and memory analysis here>>
   */
  public void initialize(ParkingGarage garage)

  {
    // insert code here
  }

  /**
   * Assigns a space to an incoming car and returns that space.
   * 
   * @param car The incoming car that needs a space.
   * @returns The space reserved for the incoming car.
   *
   * <<insert runtime and memory analysis here>>
   */
  public Space assignSpace(Car car)
  {
    // insert code here
  }

  /**
   * {@inheritDoc}
   *
   * <<insert runtime and memory analysis here>>
   */
  public void onSpaceTaken(Car car, Space space)
  {
    // insert code here
  }

  /**
   * {@inheritDoc}
   *
   * <<insert runtime and memory analysis here>>
   */
  public void onSpaceFreed(Car car, Space space)
  {
    // insert code here
  }
  
  /**
   * {@inheritDoc}
   * 
   * <<insert runtime and memory analysis here>>
   */
  void onGarageExit(Car car)
  {
    // insert code here
  }
}

/**
 * The main app controlling the parking garage.
 */
public interface ParkingGarage
{
  /**
   * Registers the given garage status listener to receive notifications for
   * changes in the occupied status of a space.
   * @param assigner The GarageStatusListener responsible for issuing spaces.
   */
  void register(GarageStatusListener assigner);

  /**
   * @return the list of spaces in the parking garage. Note: This list may be 
   * very large and take a long time to iterate through.
   */
  Iterator<Space> getSpaces();
}

/**
 * Represents a car trying to park in the parking garage.
 */
public interface Car
{
  /**
   * @return The state in which the license plate was issued.
   */
  String getLicensePlateState();

  /**
   * @return The license plate number of the car.
   */
  String getLicensePlateNumber();
}

/**
 * Represents a space in the garage in which a car can park.
 */
public interface Space
{
  /**
   * @return A unique identifier for the given space.
   */
  int getID();

  /**
   * @return An integer representing the desirability of the space.
   *         Spaces with higher values are considered more desirable.
   */
  int getDesirability(); 

  /**
   * @return true if the space is currently occupied with a car; 
   *         false, otherwise. This returns the real world state of
   *         the Space.
   */
  boolean isOccupied();

  /**
   * @return the Car that is currently occupying the Space or null
   *         if no Car is currently present. This returns the real
   *         world state of the space.
   */
  Car getOccupyingCar();
}

/**
 * An interface used to receive callbacks about changes in the status
 * of Spaces and cars in the garage. Implementers will receive notifications
 * whenever a space becomes occupied or unoccupied and whenever a car 
 * leaves the garage.
 */
public interface GarageStatusListener
{
  /**
   * Invoked whenever a car parks in a space.
   * @param car The car parking in the space.
   * @param space The space being occupied.
   */
  void onSpaceTaken(Car car, Space space);

  /**
   * Invoked whenever a car leaves a space.
   * @param car The car leaving the space.
   * @param space The space that the car left.
   */
  void onSpaceFreed(Car car, Space space);
  
  /**
   * Invoked whenever a car leaves the garage.
   * @param car The car leaving the garage.
   */
  void onGarageExit(Car car);
}
```