#include <time.h>
#include <iostream>
#include <set>
#include <list>
#include <map>
#include <string>

#if defined(DEBUG)
#define LOG_MSG printf
#else
#define LOG_MSG(...)
#endif

/***********************
 * interface definitions
************************/

/**
 * Represents a car trying to park in the parking garage.
 */
class Car
{
public:
  /**
   * @return The state in which the license plate was issued.
   */
  virtual std::string getLicensePlateState() = 0;

  /**
   * @return The license plate number of the car.
   */
  virtual std::string getLicensePlateNumber() = 0;
};

/**
 * Represents a space in the garage in which a car can park.
 */
class Space
{
public:
  /**
   * @return A unique identifier for the given space.
   */
  virtual int getID() const = 0;

  /**
   * @return An integer representing the desirability of the space.
   *         Spaces with higher values are considered more desirable.
   */
  virtual int getDesirability() = 0; 

  /**
   * @return true if the space is currently occupied with a car; 
   *         false, otherwise. This returns the real world state of
   *         the Space.
   */
  virtual bool isOccupied() const = 0;

  /**
   * @return the Car that is currently occupying the Space or null
   *         if no Car is currently present. This returns the real
   *         world state of the space.
   */
  virtual Car* getOccupyingCar() const = 0;

  // hank: I assumpted that this function is missing
  virtual void setOccupyingCar(Car* p_car) const = 0;
};

/**
 * An interface used to receive callbacks about changes in the status
 * of Spaces and cars in the garage. Implementers will receive notifications
 * whenever a space becomes occupied or unoccupied and whenever a car 
 * leaves the garage.
 */
class GarageStatusListener
{
public:
  /**
   * Invoked whenever a car parks in a space.
   * @param car The car parking in the space.
   * @param space The space being occupied.
   */
  virtual void onSpaceTaken(Car* p_car, Space* p_space) = 0;

  /**
   * Invoked whenever a car leaves a space.
   * @param car The car leaving the space.
   * @param space The space that the car left.
   */
  virtual void onSpaceFreed(Car* p_car, Space* p_space) = 0;
  
  /**
   * Invoked whenever a car leaves the garage.
   * @param car The car leaving the garage.
   */
  virtual void onGarageExit(Car* p_car) = 0;
};



/**
 * The main app controlling the parking garage.
 */
class ParkingGarage
{
public:
  /**
   * Registers the given garage status listener to receive notifications for
   * changes in the occupied status of a space.
   * @param assigner The GarageStatusListener responsible for issuing spaces.
   */
  // hank: renamed this function, since 'register' is a keyword in c++
  virtual void register_listner(GarageStatusListener* assigner) = 0;

  /**
   * @return the list of spaces in the parking garage. Note: This list may be 
   * very large and take a long time to iterate through.
   */
  virtual std::list<Space*>::iterator getSpacesBegin() = 0;
  virtual std::list<Space*>::iterator getSpacesEnd() = 0;
};

struct HigherDesirabilityFirst
{
  bool operator()(Space* a, Space* b)
  {
    return (a->getDesirability() > b->getDesirability());
  }
};

#if defined(DEBUG)
static void print_space(Space* p_space)
{
  std::cout << "[space] id: " << p_space->getID() << ", desirability: " << p_space->getDesirability() << ", occupied: " << p_space->isOccupied() << std::endl;
}

static void print_car(Car* p_car)
{
  std::cout << "[car] state: " << p_car->getLicensePlateState() << ", number: " << p_car->getLicensePlateNumber() << std::endl;
}
#endif

/**
 * The SpaceAssigner is responsible for assigning a space for an incoming
 * car to park in. This is done by calling the assignSpace() API.
 *
 * The SpaceAssigner responds to changes in space availability by 
 * implementing the GarageStatusListener interface.
 */
class SpaceAssigner : public GarageStatusListener
{
protected:
  std::multiset<Space*, HigherDesirabilityFirst> free_spaces;
  std::map<int, Space*> spaces_with_assignment;
  std::map<Car*, Space*> spaces_assigned_to_car;

  void assign_space(Car* p_car, Space* p_space)
  {
    p_space->setOccupyingCar(p_car);
    spaces_assigned_to_car.insert(std::pair<Car*, Space*>(p_car, p_space));
  }

  void release_space(Car* p_car, Space* p_space)
  {
    spaces_assigned_to_car.erase(p_car);
    p_space->setOccupyingCar(NULL);
  }

public:
  /**
   * Initiates the SpaceAssigner. This method is called only once per
   * app start-up.
   * @param garage The parking garage for which you are vending spaces.
   *
   * (Analysis)
   *
   * Time: Runs in O(logN) time where N is the number of spaces in the garage.
   *       multiset insert(): O(logN)
   *       map insert(): O(logN)
   * 
   * Memory: Allocates O(2*n) = O(n) memory where n is the number of spaces in
   * the garage. Space for TreeSet will be iteratively allocated.
   */
  void initialize(ParkingGarage* p_garage)
  {
    //  register lister
    p_garage->register_listner(this);

    //  get spaces from garage and insert them
    for(auto i = p_garage->getSpacesBegin(); i!=p_garage->getSpacesEnd(); i++)
      free_spaces.insert(*i);

    for(auto i = p_garage->getSpacesBegin(); i!=p_garage->getSpacesEnd(); i++)
      spaces_with_assignment.insert(std::pair<int, Space*>((*i)->getID(), *i));
  }

  /**
   * Assigns a space to an incoming car and returns that space.
   * 
   * @param car The incoming car that needs a space.
   * @returns The space reserved for the incoming car.
   *
   * (Analysis)
   *
   * Time: Runs in O(1) time
   *       O(n) is worst case, in case all of cars are assigned with space but none of them parked yet.
   * 
   * Memory: No new allocations
   */
  Space* assignSpace(Car* p_car)
  {
    Space* p_space = NULL;

    if(free_spaces.size() <= 0)
      return NULL;

    //  free_spaces is already sorted in desirability order
    for(auto i=free_spaces.begin(); i!=free_spaces.end(); i++) {
      p_space = spaces_with_assignment.find((*i)->getID())->second;

      if(!(p_space->isOccupied())) {
        assign_space(p_car, p_space);
        return p_space;
      }
      else {
        LOG_MSG("occupied\n");
      }
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   *
   * Time: Runs in O(logN) time where N is the number of spaces in the garage.
           find(): O(logN)
           erase(): amortized constant time
   * 
   * Memory: No new allocations
   */
  void onSpaceTaken(Car* p_car, Space* p_space)
  {
    //  refine car assignment
    //  driver may park in the Not-Assigned spot
    if(p_space->getOccupyingCar() != p_car)
      p_space->setOccupyingCar(p_car);

    //  remove this space from free space
    auto i = free_spaces.find(p_space);
    free_spaces.erase(i);
  }

  /**
   * {@inheritDoc}
   *
   * Time: Runs in O(logN) time where N is the number of spaces in the garage.
   * 
   * Memory: No new allocations
   */
  void onSpaceFreed(Car* p_car, Space* p_space)
  {
    free_spaces.insert(p_space);
    release_space(p_car, p_space);
  }
  
  /**
   * {@inheritDoc}
   *
   * Time: Runs in O(logN) time where N is the number of spaces in the garage.
   * 
   * Memory: No new allocations
   */
  void onGarageExit(Car* p_car)
  {
    std::map<Car*, Space*>::iterator i = spaces_assigned_to_car.find(p_car);
    if(i!=spaces_assigned_to_car.end()) {
      Space* p_space = i->second;
      release_space(p_car, p_space);
    }
  }

#if defined(DEBUG)
  void print()
  {
    for(auto i=free_spaces.begin(); i!=free_spaces.end(); i++) {
      Space* space_with_assignment = (Space*)spaces_with_assignment.find((*i)->getID())->second;
      print_space(space_with_assignment);
    }
  }

  void print_occupying_status()
  {
    for(auto i=free_spaces.begin(); i!=free_spaces.end(); i++) {
      Space* space_with_assignment = (Space*)spaces_with_assignment.find((*i)->getID())->second;
      if(space_with_assignment->isOccupied())
        print_space(space_with_assignment);
    }
    std::cout << "# of free spaces: " << free_spaces.size() << std::endl;
  }
#endif
};

/***********************
 * codes for test
************************/

#if defined(DEBUG)
class TestingSpace : public Space
{
public:
  TestingSpace(int id, int desirability)
  {
    this->id = id;
    this->desirability = desirability;
    this->p_occupying_car = NULL;
  }

  /**
   * @return A unique identifier for the given space.
   */
  virtual int getID() const
  {
    return id;
  }

  /**
   * @return An integer representing the desirability of the space.
   *         Spaces with higher values are considered more desirable.
   */
  virtual int getDesirability()
  {
    return desirability;
  }

  /**
   * @return true if the space is currently occupied with a car; 
   *         false, otherwise. This returns the real world state of
   *         the Space.
   */
  virtual bool isOccupied() const
  {
    return (NULL != this->getOccupyingCar());
  }

  virtual void setOccupyingCar(Car* p_car) const
  {
    this->p_occupying_car = p_car;
  }

  /**
   * @return the Car that is currently occupying the Space or null
   *         if no Car is currently present. This returns the real
   *         world state of the space.
   */
  virtual Car* getOccupyingCar() const
  {
    return p_occupying_car;
  }

protected:
  int id;
  int desirability;
  mutable Car* p_occupying_car;
};

class TestingParkingGarage : public ParkingGarage
{
protected:
  GarageStatusListener* assigner;
  std::list<Space*> spaces;

public:
  TestingParkingGarage()
  {
    assigner = NULL;
  }

  virtual void prepareTestData(int size)
  {
    srand(time(NULL));

    for(int i=0; i<size; i++) {
      spaces.push_back(new TestingSpace(i, rand()%size+1));
    }
  }

  virtual void register_listner(GarageStatusListener* assigner)
  {
    this->assigner = assigner;
  }

  virtual std::list<Space*>::iterator getSpacesBegin()
  {
    return spaces.begin();
  }

  virtual std::list<Space*>::iterator getSpacesEnd()
  {
    return spaces.end();
  }
};

class TestingCar : public Car
{
protected:
  std::string plate_state;
  std::string plate_number;

public:
  TestingCar(std::string plate_state, std::string plate_number)
  {
    this->plate_state = plate_state;
    this->plate_number = plate_number;
  }

  virtual std::string getLicensePlateState()
  {
    return this->plate_state;
  }

  virtual std::string getLicensePlateNumber()
  {
    return this->plate_number;
  }
};

class TestingCars
{
protected:
  std::list<TestingCar*> cars;

public:
  virtual void prepareTestData(int size)
  {
    for(int i=0; i<size; i++) {
      TestingCar* new_car = new TestingCar("state:"+std::to_string(i), "plate:"+std::to_string(i));
      cars.push_back(new_car);
    }
  }

  virtual std::list<TestingCar*>::iterator getCarsBegin()
  {
    return cars.begin();
  }

  virtual std::list<TestingCar*>::iterator getCarsEnd()
  {
    return cars.end();
  }
};

void test_case_0(std::string test_name)
{
  SpaceAssigner space_assigner;
  TestingParkingGarage parking_garage;
  TestingCars testing_cars;
  TestingSpace* p_space=NULL;

  std::cout << std::endl << std::endl << "<Test case: " << test_name << "> " << std::endl << std::endl;

  parking_garage.prepareTestData(50);
  space_assigner.initialize(&parking_garage);
  
  std::cout << std::endl;
  space_assigner.print();
  std::cout << std::endl;
}

void test_case_1(std::string test_name)
{
  SpaceAssigner space_assigner;
  TestingParkingGarage parking_garage;
  TestingCars testing_cars;
  TestingSpace* p_space=NULL;

  std::cout << std::endl << std::endl << "<Test case: " << test_name << "> " << std::endl << std::endl;

  parking_garage.prepareTestData(50);
  space_assigner.initialize(&parking_garage);
  
  testing_cars.prepareTestData(50);

  for(auto i=testing_cars.getCarsBegin(); i!=testing_cars.getCarsEnd(); i++) {
    TestingCar* p_car = *i;
    p_space = (TestingSpace*)space_assigner.assignSpace(p_car);
    if(!p_space){
      std::cout << "space_assigner.assignSpace() return NULL" << std::endl;
    }
    else {
      print_space(p_space);
    }

    space_assigner.onSpaceTaken(p_car, p_space);
  }
}

void test_case_2(std::string test_name)
{
  SpaceAssigner space_assigner;
  TestingParkingGarage parking_garage;
  TestingCars testing_cars;
  TestingSpace* p_space=NULL;

  std::cout << std::endl << std::endl << "<Test case: " << test_name << "> " << std::endl << std::endl;

  parking_garage.prepareTestData(5);
  space_assigner.initialize(&parking_garage);
  
  testing_cars.prepareTestData(10);

  for(auto i=testing_cars.getCarsBegin(); i!=testing_cars.getCarsEnd(); i++) {
    TestingCar* p_car = *i;
    p_space = (TestingSpace*)space_assigner.assignSpace(p_car);
    if(!p_space){
      std::cout << "no spaces are available" << std::endl;
    }
    else {
      print_space(p_space);
      space_assigner.onSpaceTaken(p_car, p_space);
    }
  }
}

int main(int argc, char* argv[])
{
  test_case_0("parking spaces are sorted in desirability order");
  test_case_1("parking spaces are vended in desirability order");
  test_case_2("return NULL when no spaces are available");

  return 0;
}
#endif
