.PHONY: all debug release

all: release debug
	
debug:
	g++ -DDEBUG -g -std=c++0x parking_lot.cc -o parking_lot_debug

release:
	g++ -std=c++0x -c parking_lot.cc

